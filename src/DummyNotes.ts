import {Note} from "./Services/NoteService";

export const DummyNotes: Note[] = [
    {
        title: "Dummy Note Please Ignore",
        content: "I told you to ignore me, didn't I?",
        id: "0",
    },
    {
        title: "I love potato knishes",
        content: "This machine makes potato knishes",
        id: "1",
    },
    {
        title: "Ratchet and Clank",
        content: "Is a good game",
        id: "2",
    },
];