import {createTheme} from "@mui/material";

namespace AppTheme {
    export const Green = "rgb(189,225,137)";
    export const Blue = "rgb(102, 163, 185)";
    export const PrimaryText = "rgb(143, 163, 173)";
    export const DarkGrey = "rgb(30, 39, 44)";
    export const BoxShadow = `
               0px 3px 1px -2px rgba(0, 0, 0, 0.5),
                0px 2px 2px 0px rgba(0, 0, 0, 0.3),
                0px 1px 5px 0px rgba(0, 0, 0, 0.35) 
            `;
    export const MaterialTheme = createTheme({
        palette: {
            mode: "dark",
        }
    })
}

export default AppTheme;