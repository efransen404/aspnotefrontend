import {useNavigate, useParams} from "react-router-dom";
import {useNavigationDialContext} from "../SharedComponents/NavigationDial";
import {useCallback, useEffect, useState} from "react";
import {CreateNoteRequest, EditNote, GetNote, Note} from "../Services/NoteService";
import {NoteForm} from "../SharedComponents/NoteForm";

export function NoteEdit() {
    const navigate = useNavigate();
    const {setActions} = useNavigationDialContext();
    const [note, setNote] = useState<Note>();
    const params = useParams();


    const editNote = useCallback((note: CreateNoteRequest) => {
        EditNote(note, params.id!).then(() => {
            navigate(`/${params.id}`)
        })
    }, [navigate, params.id])

    useEffect(() => {
        GetNote(params.id!).then(setNote);
    }, [params.id]);
    useEffect(() => {
        setActions([

        ])
    }, [setActions])

    if (!note) return null;
    return (
        <NoteForm onNoteSubmit={editNote} initialNote={note}/>
    )
}