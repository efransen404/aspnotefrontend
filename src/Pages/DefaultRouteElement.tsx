import {useNavigationDialContext} from "../SharedComponents/NavigationDial";
import {useEffect} from "react";
import {renderCreateActionButton} from "../NavigationButtons/RenderCreateActionButton";
import {useNavigate} from "react-router-dom";

export function DefaultRouteElement() {
    const {setActions} = useNavigationDialContext();
    const navigate = useNavigate();
    useEffect(() => {
        setActions(
            [renderCreateActionButton(navigate)]
              )
    }, [setActions, navigate]);
    return (
        <></>
    )
}