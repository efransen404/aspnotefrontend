import React, {useCallback, useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {Box} from "@mui/system";
import {DeleteNote, GetNote, Note} from "../Services/NoteService";
import AppTheme from "../AppTheme";
import {useNavigationDialContext} from "../SharedComponents/NavigationDial";
import {renderCreateActionButton} from "../NavigationButtons/RenderCreateActionButton";
import {renderDeleteActionButton} from "../NavigationButtons/renderDeleteActionButton";
import {renderEditActionButton} from "../NavigationButtons/renderEditActionButton";


export function NoteDisplay() {
    const navigate = useNavigate();
    const params = useParams();
    const [note, setNote] = useState<Note>();
    const {setActions} = useNavigationDialContext();

    const deleteNote = useCallback(() => {
        DeleteNote(params.id!).then(() => {
            navigate(`/`);
        });
    }, [params.id, navigate]);
    const editNote = useCallback(() => {
        navigate(`/${params.id}/edit`);
    }, [navigate, params.id])
    useEffect(() => {
        GetNote(params.id!).then(setNote)
    }, [params.id]);

    useEffect(() => {
        setActions(
            [
                renderCreateActionButton(navigate),
                renderDeleteActionButton({onClick: deleteNote}),
                renderEditActionButton({onClick: editNote}),
            ]
        )
    }, [setActions, navigate, deleteNote, editNote]);
    if (!note) return null;
    return (
        <Box sx={{
            margin: "1rem",
            textAlign: "left",
            color: AppTheme.PrimaryText,
            whiteSpace: "pre-wrap",
        }}>
            {
                note.content
            }
        </Box>
    )
}