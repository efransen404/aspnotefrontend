import React, {useCallback, useEffect, useRef} from "react";
import {Box, SxProps, Theme} from "@mui/system";
import AppTheme from "../AppTheme";
import {useNavigationDialContext} from "../SharedComponents/NavigationDial";
import {renderSaveActionButton} from "../NavigationButtons/RenderSaveActionButton";
import {useNavigate} from "react-router-dom";
import {CreateNote, CreateNoteRequest} from "../Services/NoteService";

const rootStyle: SxProps<Theme> = {
    display: "block",
    width: "100%",
    marginBottom: "10px",
    backgroundColor: "inherit",
    border: `2px solid ${AppTheme.DarkGrey}`,
    py: "5px",
    borderRadius: "8px",
    boxSizing: "border-box",
    resize: "none",
    color: AppTheme.PrimaryText,
}

export function NoteCreate() {
    const navigate = useNavigate();
    const {setActions} = useNavigationDialContext();
    const saveNote = useCallback(() => {
        if(titleRef.current == null || contentRef.current == null) return;
        let noteRequest: CreateNoteRequest = {
            title: titleRef.current.value,
            content: contentRef.current.value,
        };
        CreateNote(noteRequest).then((response)=> {
            navigate(`/${response}`);
        });
    }, [navigate])
    const titleRef = useRef<HTMLTextAreaElement>(null);
    const contentRef = useRef<HTMLTextAreaElement>(null);
    useEffect(() => {
        setActions([
            renderSaveActionButton({onClick: saveNote})
            ])
    }, [setActions, saveNote])
    return (
        <Box sx={{
            display: "flex",
            width: "100%",
            height: "100%",
            padding: "20px",
            flexDirection: "column",
            boxSizing: "border-box",
            paddingBottom: "100px",
        }}>
            <Box component="textarea" ref={titleRef} sx={rootStyle}/>
            <Box component="textarea" ref={contentRef} sx={{
                ...rootStyle,
                flexGrow: 1,
            }}/>
        </Box>
    )
}