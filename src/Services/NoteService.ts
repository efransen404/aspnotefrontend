export interface Note extends CreateNoteRequest {
    id: string;
}

export interface CreateNoteRequest {
    title: string;
    content: string;
}

// Used different syntax for practice and reference
export async function ListNotes(): Promise<Note[]> {
    let response = await fetch(`${process.env.REACT_APP_API_ROOT_ADDRESS}/note/`);
    return await response.json();
}

export function GetNote(id: string) : Promise<Note> {
    return fetch(`${process.env.REACT_APP_API_ROOT_ADDRESS}/note/${encodeURIComponent(id)}`)
        .then(response => response.json())
}

export function CreateNote(noteRequest: CreateNoteRequest) : Promise<string> {
    return fetch(`${process.env.REACT_APP_API_ROOT_ADDRESS}/note/`, {
        method: `POST`,
        body: JSON.stringify(noteRequest),
        headers: {
            'Content-Type': `application/json`,
            'Accept': 'application/json',
        },
    }).then(response => response.json())
}

export function DeleteNote(id: string) {
    return fetch(`${process.env.REACT_APP_API_ROOT_ADDRESS}/note/${encodeURIComponent(id)}`, {
        method: `DELETE`,
    })
}

export function EditNote(note: CreateNoteRequest, id: string) {
    const encodedId = encodeURIComponent(id);
    return fetch(`${process.env.REACT_APP_API_ROOT_ADDRESS}/note/${encodedId}`, {
        method: `POST`,
        body: JSON.stringify(note),
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
    })
}