import React, {FormEvent, useCallback, useEffect, useState} from "react";
import {Box, SxProps, Theme} from "@mui/system";
import AppTheme from "../AppTheme";
import {useNavigationDialContext} from "./NavigationDial";
import {renderSaveActionButton} from "../NavigationButtons/RenderSaveActionButton";
import {CreateNoteRequest} from "../Services/NoteService";

const rootStyle: SxProps<Theme> = {
    display: "block",
    width: "100%",
    marginBottom: "10px",
    backgroundColor: "inherit",
    border: `2px solid ${AppTheme.DarkGrey}`,
    py: "5px",
    borderRadius: "8px",
    boxSizing: "border-box",
    resize: "none",
    color: AppTheme.PrimaryText,
}

interface NoteFormProps {
    initialNote?: CreateNoteRequest
    onNoteSubmit(noteRequest: CreateNoteRequest): void

}

const emptyNote: CreateNoteRequest = {
    title: "",
    content: "",
}

export function NoteForm({initialNote, onNoteSubmit}: NoteFormProps) {
    const {setActions} = useNavigationDialContext();

    const [note, setNote] = useState<CreateNoteRequest>(initialNote ?? emptyNote);
    const submitNote = useCallback(() => {
        onNoteSubmit(note);
    }, [note, onNoteSubmit]);

    useEffect(() => {
        setActions([
            renderSaveActionButton({onClick: submitNote})
        ])
    }, [setActions, submitNote])

    return (
        <Box sx={{
            display: "flex",
            width: "100%",
            height: "100%",
            padding: "20px",
            flexDirection: "column",
            boxSizing: "border-box",
            paddingBottom: "100px",
        }}>
            <Box component="textarea" value={note.title} onChange={
                (e: FormEvent<HTMLTextAreaElement>) => {
                    setNote({
                        ...note,
                        title: e.currentTarget.value,
                    })
                }
            } sx={rootStyle}/>
            <Box component="textarea" value={note.content} onChange={
                (e: FormEvent<HTMLTextAreaElement>) => {
                    setNote({
                        ...note,
                        content: e.currentTarget.value,
                    })
                }
            } sx={{
                ...rootStyle,
                flexGrow: 1,
            }}/>
        </Box>
    )
}