import React, {useEffect, useState} from "react";
import {NavLink} from "react-router-dom";
import {Box} from "@mui/system";
import {ListNotes, Note} from "../Services/NoteService";
import AppTheme from "../AppTheme";

const sidebarWidth = 250;

export function Sidebar() {
    //let notes: Note[] = [];
    let [notes, setNotes] = useState<Note[]>([]);
    useEffect(()=>{
        ListNotes().then(setNotes)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [window.location.pathname]);
    return (
        <Box component={"nav"} sx={{
            overflow: "auto",
            width: sidebarWidth,
            boxSizing: "border-box",
            padding: "10px",
            backgroundColor: AppTheme.DarkGrey,
            boxShadow: `
               0px 3px 1px -2px rgba(0, 0, 0, 0.5),
                0px 2px 2px 0px rgba(0, 0, 0, 0.3),
                0px 1px 5px 0px rgba(0, 0, 0, 0.35) 
            `
        }}>
            <Box component={"ul"} sx={{
                listStyle: "none",
                paddingLeft: "0px",
            }}>
                {
                    notes
                        .map(note => <ListItem note={note} key={note.id}/>)
                }
            </Box>
        </Box>
    );
}

interface ListItemProps {
    note: Note
}

function ListItem(props: ListItemProps) {
    return (
        <Box component={"li"} sx={{
            marginBottom: "1rem",
        }}>
            <NavLink style={({isActive}) => ({
                color: isActive ? AppTheme.Green : AppTheme.Blue,
                textDecoration: "none"
            })} to={`/${props.note.id}`}>{props.note.title}</NavLink>
        </Box>
    );
}