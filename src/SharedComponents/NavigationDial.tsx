import React, {createContext, ReactNode, useContext} from "react";
import {SpeedDial} from "@mui/material";

import MenuIcon from '@mui/icons-material/Menu';

interface NavigationDialContextData {
    actions: ReactNode;
    setActions(actions: ReactNode[]): void;
}
export const NavigationDialContext = createContext<NavigationDialContextData>(undefined!);
export const useNavigationDialContext = () => useContext(NavigationDialContext)

export function NavigationDial() {
    const {actions} = useNavigationDialContext();
    return (
        <SpeedDial
            ariaLabel="Navigation Dial"
            icon={<MenuIcon/>}
            sx={{
                position: "absolute",
                bottom: 32,
                right: 32,
            }}
        >
            {actions}
        </SpeedDial>
    )
}