import {SpeedDialAction} from "@mui/material";
import EditIcon from '@mui/icons-material/Edit';

interface EditActionButtonProps {
    onClick(): void
}

export function renderEditActionButton({onClick}: EditActionButtonProps) {
    return (
        <SpeedDialAction
            icon={<EditIcon/>}
            tooltipTitle="Edit Note"
            onClick={onClick}
            key="edit"
        />
    )
}