import {SpeedDialAction} from "@mui/material";

import AddIcon from '@mui/icons-material/Add';
import {NavigateFunction} from "react-router-dom";

export function renderCreateActionButton(navigate: NavigateFunction) {
    return (
        <SpeedDialAction
            icon={<AddIcon/>}
            tooltipTitle={"Create Note"}
            onClick={() => {
                navigate(`/create`);
            }}
            key="create"
        />
    )
}