import {SpeedDialAction} from "@mui/material";

import DeleteIcon from '@mui/icons-material/Delete';

interface DeleteActionButtonProps {
    onClick(): void
}

export function renderDeleteActionButton({onClick}: DeleteActionButtonProps) {
    return(
        <SpeedDialAction
            icon={<DeleteIcon/>}
            tooltipTitle="Delete note"
            onClick={onClick}
            key="delete"
            />
    )
}