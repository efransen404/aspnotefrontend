import {SpeedDialAction} from "@mui/material";

import SaveIcon from '@mui/icons-material/Save';

interface SaveActionButtonProps {
    onClick(): void
}
export function renderSaveActionButton({onClick}: SaveActionButtonProps) {
    return(
        <SpeedDialAction
        icon={<SaveIcon/>}
        tooltipTitle="Save Note"
        onClick={onClick}
        key="save"
        />
    )
}