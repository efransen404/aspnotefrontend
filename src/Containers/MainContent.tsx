import React from "react";
import {NoteDisplay} from "../Pages/NoteDisplay";
import {Route, Routes} from 'react-router-dom';
import {Box} from "@mui/system";
import {NoteCreate} from "../Pages/NoteCreate";
import {DefaultRouteElement} from "../Pages/DefaultRouteElement";
import {NoteEdit} from "../Pages/NoteEdit";

export function MainContent() {
    return (
        <Box component={"main"} sx={{
            flexGrow: 1,
        }}>
            <Routes>
                <Route path="/:id/edit" element={<NoteEdit/>}/>
                <Route path="/:id" element={<NoteDisplay/>}/>
                <Route path="/create" element={<NoteCreate/>}/>
                <Route index element={<DefaultRouteElement/>}/>

            </Routes>
        </Box>
    )
}

