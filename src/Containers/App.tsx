import React, {ReactNode, useState} from 'react';
import {Sidebar} from "../SharedComponents/Sidebar";
import {MainContent} from "./MainContent";
import {BrowserRouter} from "react-router-dom";
import {Box} from "@mui/system";
import {NavigationDial, NavigationDialContext} from "../SharedComponents/NavigationDial";
import {ThemeProvider} from "@mui/material";
import AppTheme from "../AppTheme";

function AppContent() {
    return (
        <Box sx={{
            display: "flex",
            width: "100%",
            height: "100%",
        }}>
            <Sidebar/>
            <MainContent/>
            <NavigationDial/>
        </Box>);
}

function App() {
    const [actions, setActions] = useState<ReactNode>();
    return (
        <ThemeProvider theme={AppTheme.MaterialTheme}>
            <BrowserRouter>
                <NavigationDialContext.Provider value={{actions, setActions}}>
                    <AppContent/>
                </NavigationDialContext.Provider>
            </BrowserRouter>
        </ThemeProvider>
    );
}

export default App;
