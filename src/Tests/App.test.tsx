import React from 'react';
import { render, screen } from '@testing-library/react';
import * as NoteService from "../Services/NoteService";
import App from '../Containers/App';
import userEvent from "@testing-library/user-event";
import {DummyNotes} from "../DummyNotes";

jest.mock("./NoteService");
const NoteServiceMock = NoteService as unknown as jest.Mocked<typeof NoteService>;

test('Clicking on link opens correct note', async () => {
  NoteServiceMock.ListNotes.mockResolvedValue(DummyNotes);
  NoteServiceMock.GetNote.mockResolvedValue(DummyNotes[0]);
  render(<App/>);
  const element = await screen.findByText(DummyNotes[0].title);

  userEvent.click(element);
  const mainElement = await screen.findByText(DummyNotes[0].content);

  expect(mainElement).toBeInTheDocument();
  expect(NoteServiceMock.GetNote).toHaveBeenCalledWith(DummyNotes[0].id);
});
