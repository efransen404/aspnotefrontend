import {render, screen} from "@testing-library/react";
import {BrowserRouter} from "react-router-dom";
import React from "react";
import {Sidebar} from "../SharedComponents/Sidebar";
import * as NoteService from "../Services/NoteService";
import {DummyNotes} from "../DummyNotes";

jest.mock("./NoteService");
const NoteServiceMock = NoteService as unknown as jest.Mocked<typeof NoteService>;

beforeEach(() => {
    NoteServiceMock.ListNotes.mockResolvedValue(DummyNotes);
});

test('shows correct note as active', async () => {
    window.history.replaceState({}, "", "/0");
    render(<BrowserRouter>
        <Sidebar/>
    </BrowserRouter>)
    const element = await screen.findByText("Dummy Note Please Ignore");
    expect(element).toHaveStyle("color: rgb(189,225,137)");
});

test('shows inactive note as inactive', async () => {
    window.history.replaceState({}, "", "/0");
    render(<BrowserRouter>
        <Sidebar/>
    </BrowserRouter>)
    const element = await screen.findByText("I love potato knishes");
    expect(element).toHaveStyle("color: rgb(102, 163, 185)");
});