import React from 'react';
import { render, screen } from '@testing-library/react';
import {BrowserRouter} from "react-router-dom";
import {MainContent} from "../Containers/MainContent";
import * as NoteService from "../Services/NoteService";
import {DummyNotes} from "../DummyNotes";

jest.mock("./NoteService");
const NoteServiceMock = NoteService as unknown as jest.Mocked<typeof NoteService>;

test('renders first dummy note content', async () => {
    NoteServiceMock.GetNote.mockResolvedValue(DummyNotes[0])
    window.history.replaceState({}, "", `/${DummyNotes[0].id}`);
    render(<BrowserRouter>
        <MainContent/>
    </BrowserRouter>);
    const element = await screen.findByText("I told you to ignore me, didn't I?");
    expect(element).toBeInTheDocument();
});

test('renders no content with no route', () => {
    window.history.replaceState({}, "", "/");
    render(<BrowserRouter>
        <MainContent/>
    </BrowserRouter>);
    expect(screen.getByRole("main")).toBeEmptyDOMElement();
});

test('renders no content with invalid route', () => {
    window.history.replaceState({}, "", "/3");
    render(<BrowserRouter>
        <MainContent/>
    </BrowserRouter>);
    expect(screen.getByRole("main")).toBeEmptyDOMElement();
});



